import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

public class TryWithResources_Example {
public static void main(String args[]) {
   String mysqlUrl = "jdbc:mysql://localhost/mydatabase";
   System.out.println("Connection established......");
   Statement stmt;
   try (Connection con = DriverManager.getConnection(mysqlUrl, "root", "password")){
      stmt = con.createStatement();
   } catch (SQLException e) {
      e.printStackTrace();
   } finally {
      stmt.close();
   }
}
}
